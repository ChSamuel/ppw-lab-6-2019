from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def index(request):
    if(request.method == 'POST'):
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')

    newForm = StatusForm()
    isiModel = Status.objects.all()

    return render(request, 'index.html', {'form':newForm, 'model':isiModel})